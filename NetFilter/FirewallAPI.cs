﻿using NetFwTypeLib;

using System;
using System.Collections.Generic;
using System.Text;

namespace NetFilter
{
    public class FirewallAPI : IFirewallAPI
    {
        public FirewallAPI()
        {
            NetFwMgr = (INetFwMgr)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwMgr"));
            NetFwPolicy2 = (INetFwPolicy2)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwPolicy2"));
        }

        public INetFwMgr NetFwMgr { get; }

        public INetFwPolicy2 NetFwPolicy2 { get; }

        public static INetFwRule CreateFwRule(Action<INetFwRule> setter)
        {
            INetFwRule _netFwRule3 = (INetFwRule)Activator.CreateInstance(Type.GetTypeFromProgID("HNetCfg.FwRule"));
            setter?.Invoke(_netFwRule3);
            return _netFwRule3;
        }

    }

    public interface IFirewallAPI
    {
        INetFwMgr NetFwMgr { get; }

        INetFwPolicy2 NetFwPolicy2 { get; }

    }
}
