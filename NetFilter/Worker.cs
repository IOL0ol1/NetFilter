using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using NetFwTypeLib;

using NLog.Filters;

using SharpPcap;

using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace NetFilter
{
    public class Worker : BackgroundService
    {
        private const string RuleName = "Anti-DDOS";

        private readonly ILogger<Worker> _logger;
        private readonly IFirewallAPI _firewallAPI;
        private readonly IConfigurationRoot _configurationRoot;
        private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ConcurrentStack<RawCapture> stack = new ConcurrentStack<RawCapture>();
        private readonly object sync = new object();
        private EventWaitHandle eventWaitHandle = new AutoResetEvent(false);
        private ICaptureDevice device;

        public Worker(ILogger<Worker> logger, IFirewallAPI firewallAPI, IConfigurationRoot configurationRoot)
        {
            _logger = logger;
            _firewallAPI = firewallAPI;
            _configurationRoot = configurationRoot;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation($"SharpPcap {SharpPcap.Version.VersionString}");
            _logger.LogInformation("Worker running");
            await Task.Run(NPcapFilter, stoppingToken);
        }

        /// <summary>
        /// mian filter by pcap
        /// </summary>
        private void NPcapFilter()
        {
            var devices = CaptureDeviceList.Instance;
            for (int i = 0; i < devices.Count; i++)
            {
                _logger.LogInformation($"{i} {devices[i].Description}");
            }
            var deviceIndex = int.Parse(_configurationRoot.GetSection("DeviceIndex").Value);
            if (deviceIndex < 0)
            {
                _logger.LogInformation($"Current DeviceIndex = {deviceIndex}, change DeviceIndex in appsettings.json and restart the software");
                return;
            }
            MemoryCheckTask(100 * 1024 * 1024, cancellationTokenSource.Token); // 内存占用警告关闭
            NetFilterTask(cancellationTokenSource.Token); // 过滤线程

            device = devices[deviceIndex];
            device.OnPacketArrival += Device_OnPacketArrival;
            device.Open(DeviceMode.Normal);
            device.Filter = _configurationRoot.GetSection("NpcapFilter").Value;
            device.Capture();
        }


        public override Task StopAsync(CancellationToken cancellationToken)
        {
            cancellationTokenSource.Cancel();
            device?.Close();
            eventWaitHandle.Close();
            return base.StopAsync(cancellationToken);
        }

        /// <summary>
        /// filter callback
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Device_OnPacketArrival(object sender, CaptureEventArgs e)
        {
            stack.Push(e.Packet);
            eventWaitHandle.Set();
        }

        /// <summary>
        /// log memory size each 10 second
        /// </summary>
        /// <param name="maxBytes"></param>
        /// <param name="cancellation"></param>
        /// <returns></returns>
        private Task MemoryCheckTask(int maxBytes, CancellationToken cancellation)
        {
            return Task.Run(async () =>
            {
                try
                {
                    while (!cancellation.IsCancellationRequested)
                    {
                        await Task.Delay((int)TimeSpan.FromSeconds(10).TotalMilliseconds, cancellation);
                        var privateMemory = Process.GetCurrentProcess().PrivateMemorySize64;
                        _logger.LogInformation($"Current memory: {privateMemory}");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, nameof(MemoryCheckTask));
                }
            });
        }

        /// <summary>
        /// process packet stack
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private Task NetFilterTask(CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    if (stack.TryPop(out var rawCapture))
                    {
                        var packet = PacketDotNet.Packet.ParsePacket(rawCapture.LinkLayerType, rawCapture.Data);
                        if (packet is PacketDotNet.EthernetPacket)
                        {
                            var ip = packet.Extract<PacketDotNet.IPPacket>();
                            if (ip != null)
                            {
                                var udp = packet.Extract<PacketDotNet.UdpPacket>();
                                if (udp != null)
                                {
                                    var srcAddress = ip.SourceAddress;
                                    var srcPort = udp.SourcePort;
                                    var dstAddress = ip.DestinationAddress;
                                    var dstPort = udp.DestinationPort;
                                    AddRule(srcAddress, srcPort, dstAddress, dstPort);
                                }
                            }
                        }
                        stack.Clear();
                    }
                    else
                    {
                        eventWaitHandle.WaitOne();
                    }
                }
            }, cancellationToken);
        }

        /// <summary>
        /// add block ip to windows firewall
        /// </summary>
        /// <param name="srcAddress"></param>
        /// <param name="srcPort"></param>
        /// <param name="dstAddress"></param>
        /// <param name="dstPort"></param>
        private void AddRule(IPAddress srcAddress, int srcPort, IPAddress dstAddress, int dstPort)
        {
            var localPorts = _configurationRoot.GetSection("LocalPorts").Value; // udp local port in config
            if (_firewallAPI.NetFwPolicy2.Rules.Item(RuleName) is INetFwRule rule)
            {
                // if rule is exit, skip
                if (rule.RemoteAddresses.Contains($"{srcAddress}") && rule.LocalPorts == localPorts)
                {
                    _logger.LogInformation($"Rule has exits: src {srcAddress}:{srcPort} => dst {dstAddress}:{dstPort}");
                    return;
                }
                rule.LocalPorts = localPorts; // udp local port
                rule.RemoteAddresses += $",{srcAddress}"; // add remote ip address
            }
            else
            {
                _firewallAPI.NetFwPolicy2.Rules.Add(FirewallAPI.CreateFwRule(_ =>
                {
                    _.Name = RuleName;
                    _.Enabled = true;
                    _.Direction = NET_FW_RULE_DIRECTION_.NET_FW_RULE_DIR_IN; // in bound
                    _.Action = NET_FW_ACTION_.NET_FW_ACTION_BLOCK; // block
                    _.InterfaceTypes = "All"; // all net interface
                    _.Protocol = 17; // udp protocol
                    _.LocalPorts = localPorts; // udp local port
                    _.RemoteAddresses = $"{srcAddress}"; // remote ip address
                }));
            }
            _logger.LogWarning($"Anti-DDOS action: src {srcAddress}:{srcPort} => dst {dstAddress}:{dstPort}");
        }
    }
}
